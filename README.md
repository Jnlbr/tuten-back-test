# TUTEN backend test

This API was made in Java with Spring, you can test it on https://tuten-backend-test.herokuapp.com/

## Libraries

| Name      | Version |
| --------- | :-----: |
| Joda-time | 2.10.5  |
| Thymeleaf |  3.0.4  |

## Steps for building the app

1. In the root folder run `./gradlew build` to generate a .JAR file
2. Run `java -jar build/libs/JAR_FILE_NAME.jar` to start the service

## Endpoints summary

| Route | Method |    Body    | Params |    Response    | Description                                |
| ----- | :----: | :--------: | :----: | :------------: | ------------------------------------------ |
| /     |  GET   |            |        |      HTML      | Get index template                         |
| /utc  |  POST  | UTCBodyDTO |        | UTCResponseDTO | Convert the given time and timezone to UTC |
| /utc  |  GET   |            |        |     String     | Get current datetime in UTC                |

## DTO's example

#### UTCBodyDTO

`{ time: String, timezone: Integer }`

#### UTCResponseDTO

`{ time: String, timezone: String }`
