package com.test.tuten.controllers;

import com.test.tuten.dtos.ResponseDTO;
import com.test.tuten.dtos.UTCBodyDTO;
import com.test.tuten.dtos.UTCResponseDTO;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.web.bind.annotation.*;


@RestController
public class MainController {

    private String shortTime = "HH:mm:ss";

    @PostMapping("/utc")
    public ResponseDTO generateUTCFormat(@RequestBody UTCBodyDTO body) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern(shortTime);
        fmt = fmt.withZone(DateTimeZone.forOffsetHours(body.getTimezone() * -1));
        DateTime dateTime = fmt.parseDateTime(body.getTime());
        dateTime = dateTime.toDateTime(DateTimeZone.UTC);
        dateTime = dateTime.withDate(LocalDate.now());
        return new ResponseDTO(new UTCResponseDTO(dateTime.toString(), "UTC"));
    }


    @GetMapping("/utc")
    public ResponseDTO getUTCFormat() {
        DateTime dateTime = new DateTime();
        dateTime = dateTime.toDateTime(DateTimeZone.UTC);
        return new ResponseDTO(dateTime.toString());
    }

}
