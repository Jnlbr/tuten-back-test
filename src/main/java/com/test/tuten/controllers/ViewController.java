package com.test.tuten.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ViewController {

    @GetMapping({"/"})
    public String index(Model model) {
        model.addAttribute("name", "Jean");
        return "index";
    }
}
