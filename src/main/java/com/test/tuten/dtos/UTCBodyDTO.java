package com.test.tuten.dtos;

public class UTCBodyDTO {
    private String time;
    private Integer timezone;

    public UTCBodyDTO(String time, Integer timezone) {
        this.time = time;
        this.timezone = timezone;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getTimezone() {
        return timezone;
    }

    public void setTimezone(Integer timezone) {
        this.timezone = timezone;
    }
}
