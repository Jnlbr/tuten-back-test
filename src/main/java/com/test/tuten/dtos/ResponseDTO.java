package com.test.tuten.dtos;

public class ResponseDTO {
    Object response;

    public ResponseDTO(Object response) {
        this.response = response;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

}
