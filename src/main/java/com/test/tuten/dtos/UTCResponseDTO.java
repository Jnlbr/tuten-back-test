package com.test.tuten.dtos;

public class UTCResponseDTO {
    public UTCResponseDTO(String time, String timezone) {
        this.time = time;
        this.timezone = timezone;
    }

    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    private String timezone;

}
